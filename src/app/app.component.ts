import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import {Stomp, Client, Message} from 'stompjs/lib/stomp.js';
import {SockJS} from 'sockjs-client';
import * as Socket from 'socket.io-client';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  stompClient;
  values = [];
  serverUrl = 'http://test-upfront-security.com:7050/ws';
  jsonURL = '/assets/JSONarray.json';

  insertingData = [
    {
      ID: 1,
      Messages: [
        {
          Unique_id: 'yf',
          receiverType: 'I',
          sender_name: 'febin',
          sender_id: 'f1',
          receiver_name: 'yasi',
          receiver_id: 'y1',
          message_type: 'TEXT',
          message: 'Hello',
          parent: 0,
          time: 6888768
        }
      ],
      child: []
    },
    {
      ID: 2,
      Messages: [
        {
          Unique_id: 'yf',
          receiverType: 'I',
          sender_name: 'febin',
          sender_id: 'f1',
          receiver_name: 'yasi',
          receiver_id: 'y1',
          message_type: 'TEXT',
          message: 'Akshatha',
          parent: 0,
          time: 6888768
        },
      ],
      child: []
    },
    {
      ID: 3,
      Messages: [
        {
          Unique_id: 'yf',
          receiverType: 'I',
          sender_name: 'febin',
          sender_id: 'f1',
          receiver_name: 'yasi',
          receiver_id: 'y1',
          message_type: 'TEXT',
          message: 'Ravindran',
          parent: 1,
          time: 6888768
        }
      ],
      child: []
    },
    {
      ID: 4,
      Messages: [
        {
          Unique_id: 'yf',
          receiverType: 'I',
          sender_name: 'febin',
          sender_id: 'f1',
          receiver_name: 'yasi',
          receiver_id: 'y1',
          message_type: 'TEXT',
          message: 'Jaswanth',
          parent: 2,
          time: 6888768
        }
      ],
      child: []
    },
    {
      ID: 5,
      Messages: [
        {
          Unique_id: 'yf',
          receiverType: 'I',
          sender_name: 'febin',
          sender_id: 'f1',
          receiver_name: 'yasi',
          receiver_id: 'y1',
          message_type: 'TEXT',
          message: 'Jaswanth',
          parent: 2,
          time: 6888768
        }
      ],
      child: []
    },
    {
      ID: 6,
      Messages: [
        {
          Unique_id: 'yf',
          receiverType: 'I',
          sender_name: 'febin',
          sender_id: 'f1',
          receiver_name: 'yasi',
          receiver_id: 'y1',
          message_type: 'TEXT',
          message: 'Jaswanth',
          parent: 4,
          time: 6888768
        }
      ],
      child: []
    },
  ];

/*  testDataArray = [
    {id: 1, title: 'hello', parent: 0},
    {id: 2, title: 'hello', parent: 0},
    {id: 3, title: 'hello', parent: 1},
    {id: 4, title: 'hello', parent: 3},
    {id: 5, title: 'hello', parent: 4},
    {id: 6, title: 'hello', parent: 4},
    {id: 7, title: 'hello', parent: 3},
    {id: 8, title: 'hello', parent: 2}
  ];*/

  testDataArrayTestkhjkjh = [
    {
      ID: 1,
      Messages: [
        {
          Unique_id: 'yf',
          receiverType: 'I',
          sender_name: 'febin',
          sender_id: 'f1',
          receiver_name: 'yasi',
          receiver_id: 'y1',
          message_type: 'TEXT',
          message: 'Hello',
          parent: 0,
          time: 6888768
        }
      ],
      child: []
    },
    {
      ID: 2,
      Messages: [
        {
          Unique_id: 'yf',
          receiverType: 'I',
          sender_name: 'febin',
          sender_id: 'f1',
          receiver_name: 'yasi',
          receiver_id: 'y1',
          message_type: 'TEXT',
          message: 'Akshatha',
          parent: 1,
          time: 6888768
        }
      ],
      child: []
    },
    {
      ID: 3,
      Messages: [
        {
          Unique_id: 'yf',
          receiverType: 'I',
          sender_name: 'febin',
          sender_id: 'f1',
          receiver_name: 'yasi',
          receiver_id: 'y1',
          message_type: 'TEXT',
          message: 'Ravindran',
          parent: 1,
          time: 6888768
        }
      ],
      child: []
    },
    {
      ID: 4,
      Messages: [
        {
          Unique_id: 'yf',
          receiverType: 'I',
          sender_name: 'febin',
          sender_id: 'f1',
          receiver_name: 'yasi',
          receiver_id: 'y1',
          message_type: 'TEXT',
          message: 'Jaswanth',
          parent: 2,
          time: 6888768
        }
      ],
      child: []
    },
    {
      ID: 5,
      Messages: [
        {
          Unique_id: 'yf',
          receiverType: 'I',
          sender_name: 'febin',
          sender_id: 'f1',
          receiver_name: 'yasi',
          receiver_id: 'y1',
          message_type: 'TEXT',
          message: 'Jaswanth',
          parent: 4,
          time: 6888768
        }
      ],
      child: []
    }
  ];

  finalMessagingMap = new Map();

  existingMsgArray = [];

  constructor(private http: HttpClient) {}

  ngOnInit() {
    // this.getNestedChildrenTest(this.testDataArrayTest, 0);
    this.getNestedChildrenTestTest();
  }

  jsonAppend() {
    this.http.get(this.jsonURL).subscribe(result => {
      this.values = result as string[];
      // let appendedChildArray = [];
      // for (let i = 0; i < this.insertingData.length; i++) {
      //     for (let j = 0; j < this.values.length; j++) {
      //       if (this.insertingData[i].Messages[0].parent === this.values[j].ID) {
      //         this.values[j].child.unshift(JSON.parse(JSON.stringify(this.insertingData[i])));
      //         appendedChildArray = this.values[j].child;
      //       } else {
      //         for(let k = 0; k < appendedChildArray.length; k++) {
      //           if (this.insertingData[i].Messages[0].parent === appendedChildArray[k].ID) {
      //             appendedChildArray[k].child.unshift(JSON.parse(JSON.stringify(this.insertingData[i])));
      //           }
      //         }
      //       }
      //     }
      //   }
      //   console.log(this.values);
      // for (let i = 0; i < this.insertingData.length; i++) {
      this.getNestedChildren(this.values, this.insertingData[0].Messages[0].parent);
      // console.log(this.insertingData[k].Messages[0].parent);
      // }
    }, error => console.error(error));
  }

  getNestedChildren(arr, parent) {
    let children = [];
    let output = [];
    let child2;
    let child1 = [];
    for (var i in arr) {
      for (var j in this.insertingData) {
        if (arr[i].ID === parent) {
          children = this.getNestedChildren(arr[0], this.insertingData[j].Messages[0].parent);
          console.log(children['child']);
          if (children['ID'] === this.insertingData[j].Messages[0].parent) {
            arr[i].child.unshift(this.insertingData[j]);
          }
          for (let k in arr[0].child) {
            child1 = this.getNestedChildren(arr[0].child[k], this.insertingData[j].Messages[0].parent);
            console.log(child1);
            if (child1['ID'] === this.insertingData[j].Messages[0].parent) {
              arr[i].child[k].child.unshift(this.insertingData[j]);
            }

          }
        }
      }
    }
    console.log(arr);
    return arr;
  }

  connectScoket() {
    // const socket = new SockJS('http://localhost:7054/ws');
    const socket = Socket(this.serverUrl);
    this.stompClient = Stomp.over(socket);
    let that = this;
    this.stompClient.connect({}, this.onConnected(), this.onError());
    event.preventDefault();
  }

  onConnected() {
    let UserID;
    UserID = (<HTMLInputElement>document.getElementById('UserID')).value;
    document.getElementById('connected').innerHTML = 'Connected to scocket';
    this.stompClient.subscribe('/topic/' + UserID, function (payload) {
      const message = JSON.parse(payload.body);
      // addMsg(message);
      const myJSONString = JSON.stringify(message);
      document.getElementById('receivedMsg').innerHTML = myJSONString;
    });
  }

  onError() {
    // connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    // connectingElement.style.color = 'red';
  }

  ///////////////////////////////////////////////////////////// Jaswanth TESTING/////////////////////////////////////////////////////////////////
 /* getNestedChildrenTest(arr, parent) {
    const out = [];
    for (const i in arr) {
      if (arr[i].parent === parent) {
        this.children = this.getNestedChildrenTest(arr, arr[i].id);

        if (this.children.length) {
          arr[i].children = this.children;
        }
        out.push(arr[i]);
      }
    }
    return out;
  }*/

  getNestedChildrenTestTest() {

    for (let j = 0; j < this.testDataArrayTestkhjkjh.length; j++) {
      console.log(this.testDataArrayTestkhjkjh[j].Messages[0].parent);
      console.log(this.finalMessagingMap.has(this.testDataArrayTestkhjkjh[j].Messages[0].parent));

      // this is a array to maintain the data for a particular instance.
      this.existingMsgArray = [];

      // Check whether the Parent ID is already present in the MAP
      // this has() method will chech whether the passed key is already present or not on in the map. (it gives either true / false )
      if (this.finalMessagingMap.has(this.testDataArrayTestkhjkjh[j].Messages[0].parent)) {
        // if we have the key id already in the map then get the values of the key and assign it to the array.
        this.existingMsgArray = this.finalMessagingMap.get(this.testDataArrayTestkhjkjh[j].Messages[0].parent);
        // now the new object can be pushed into the existing array so that older data is not lost.
        this.existingMsgArray.push(this.testDataArrayTestkhjkjh[j]);
        // And finally the array data is set into the MAP to the respective ID.
        this.finalMessagingMap.set(this.testDataArrayTestkhjkjh[j].Messages[0].parent, this.existingMsgArray);
      } else {
        // if the parent id is not present in the MAP then the object is pushed into an array and set to the ID in the MAP.
        this.existingMsgArray.push(this.testDataArrayTestkhjkjh[j]);
        this.finalMessagingMap.set(this.testDataArrayTestkhjkjh[j].Messages[0].parent, this.existingMsgArray);
      }
    }
    console.log(this.finalMessagingMap);
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
